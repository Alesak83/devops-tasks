# Výuková aplikace pro Docker Compose

Zadání jednotlivých úloh najdete v [/docs](docs), příp. můžete využít přímého odkazu zde:

1. [úloha Docker](docs/01-zadani-docker.md)
2. [úloha Docker Compose](docs/02-zadani-docker-compose.md)
